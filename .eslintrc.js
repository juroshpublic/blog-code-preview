const path = require('path');

module.exports = {
  extends: [
    "react-app",
    "plugin:import/errors",
    "plugin:import/warnings"
  ],
  plugins: [
    "import"
  ],
  settings: {
    "import/resolver": {
      webpack: {
        config: { resolve: { modules: [ path.resolve(__dirname, './src') ] } }
      }
    }
  }
}

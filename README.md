# JuroshBiz Blog 

Code preview repository of simple BLOG system.

Code is functional, connected to 'fake api' as offline temporary "database" without using real backend.

- Run with: `npm start`
- Test with: `npm test`
- Check JavaScript with: `npm run lint`
- Check CSS with: `npm run lintcss`
- Run build with: `npm run build`

# Tech details

- UI rendering - React (https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
- Middleware (side effects) - Redux Saga.
- Pure application state - Redux.
- API - firebase / fake

*Note: to switch from firebase to fake API modify files:*
- app/index.js: change import `app/api/api` to `app/apiFake/api`
- app/redux/middlewareSagas.js: change import `app/api/posts` to `app/apiFake/posts`

Author: **Juraj Husar**

&copy; 2016-2017 Do not copy any code of this repo without author's permission.

export default [
  {
    "content": "# Some nice heading (markdown)",
    "created": 1488029479238,
    "postId": "9e2eba5d-e99f-d6d3-5107-020db9959e31",
    "postState": "published",
    "title": "Cool title!",
    "updated": 1488139738598,
    "url": "some-nice-post"
  },
  {
    "content": "# Some another heading (markdown)",
    "created": 1485029479238,
    "postId": "8e2eba5d-e99f-d6d3-5107-020db9959e31",
    "postState": "published",
    "title": "Next article",
    "updated": 1488135738598,
    "url": "some-nice-post-2"
  },
  {
    "content": "# Some last heading (markdown)",
    "created": 1487029479238,
    "postId": "7e2eba5d-e99f-d6d3-5107-020db9959e31",
    "postState": "published",
    "title": "Another article title",
    "updated": 1488139738598,
    "url": "some-nice-post-3"
  }
]
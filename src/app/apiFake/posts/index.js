import { guid } from 'app/utils/string';
import { normalizePosts } from 'app/api/posts/schema';
import { checkAndCorrectPost } from 'app/api/posts';
import postsJson from './posts.data';

export const fetchBlogPosts = () => {
  postsJson.sort((a,b) => a.created > b.created);
  return normalizePosts(postsJson);
}

export const updateBlogPost = (existing, updated) => {
  const updatedObj = checkAndCorrectPost({
    ...existing,
    ...updated
  });
  let existingRef = postsJson.find(post => post.postId === existing.postId);
  existingRef = updatedObj;
  return existingRef;
}

export const createBlogPost = (data) => {
  const newPost = checkAndCorrectPost({
    postState: 'draft',
    ...data,
    postId: guid(),
    created: new Date().getTime()
  });
  postsJson.push(newPost);
  return newPost;
};

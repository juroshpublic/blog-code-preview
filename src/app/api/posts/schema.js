import { normalize, schema } from 'normalizr';

const post = new schema.Entity('posts', {}, {idAttribute: 'postId'});

export const normalizePosts = (listing) => normalize(listing, [ post ]);

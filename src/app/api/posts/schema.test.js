import {normalizePosts} from './schema';

test('normalizing of posts - good path', () => {
  const firstEntity = {
    postId: 1,
    name: 'First'
  };
  const secondEntity = {
    postId: 2,
    name: 'Second'
  };
  const posts = [firstEntity, secondEntity];
  expect(normalizePosts(posts)).toEqual({
    entities: {
      posts: {
        "1": firstEntity,
        "2": secondEntity
      }
    },
    result: [1, 2]
  });
});

import React from 'react';
import MaterialButton from './MaterialButton';
import { shallow } from 'enzyme';

test('renders button properly', () => {
  const button = shallow(<MaterialButton className="my_Buton" />);
  expect(button.find('.my_Buton').exists()).toEqual(true);
  expect(button.find('.MaterialButton-styled').exists()).toEqual(true);
});

test('renders styles button properly', () => {
  const button = shallow(<MaterialButton styled />);
  expect(button.find('.MaterialButton-styled').exists()).toEqual(true);
});

test('button is triggerng action', () => {
  const clickMock = jest.fn();
  const button = shallow(<MaterialButton onClick={clickMock} />);
  expect(button.find('.MaterialButton-pushed').exists()).toEqual(false);
  // Click that button
  button.simulate('click');
  expect(clickMock.mock.calls.length).toBe(1);
});

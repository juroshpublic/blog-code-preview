import { connect } from 'react-redux';
import PostSimpleListing from './PostSimpleListing';

const mapStateToProps = (state, ownProps) => {
  const posts = state.posts.listing || [];
  return {
    posts
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  // empty
});

const PostListingContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PostSimpleListing);

export default PostListingContainer;
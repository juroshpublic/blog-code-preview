import React from 'react';
import PostContainer from './PostContainer';

export default function PostSimpleListing({ posts }) {
  return (
    <div>
      {posts.map(postId =>
        <PostContainer key={postId} id={postId} />
      )}
    </div>
  );
}

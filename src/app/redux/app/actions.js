import * as actions from 'app/redux/actionTypes';

export const loggIn = () => ({
  type: actions.LOGGIN
});

export const loggInError = () => ({
  type: actions.LOGGIN_ERROR
});
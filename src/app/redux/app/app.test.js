import { state } from './app';
import * as actions from 'app/redux/actionTypes';

test('reducer: app > state - user logged in', () => {
  const action = {
    type: actions.LOGGIN
  };
  expect(state({}, action)).toEqual({loading: false, error: false});
});

test('reducer: app > state - user loggin error', () => {
  const action = {
    type: actions.LOGGIN_ERROR
  };
  expect(state({}, action)).toEqual({loading: false, error: true});
});

test('reducer: app > state - should not touch state when not related action', () => {
  const action = {
    type: 'UNKNOWN'
  };
  expect(state({test: true}, action)).toEqual({test: true});
});
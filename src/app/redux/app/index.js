import { combineReducers } from 'redux';
import * as appReducers from './app';

export default combineReducers({
  ...appReducers
});
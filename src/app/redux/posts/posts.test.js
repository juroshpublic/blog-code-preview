import { listing, editState, editing } from './posts';
import * as actions from 'app/redux/actionTypes';

test('reducer: posts > listing - fetched blog posts', () => {
  const action = {
    type: actions.FETCHED_BLOG_POSTS,
    result: [3, 4]
  };
  expect(listing([1], action)).toEqual([3, 4]);
});

test('reducer: posts > listing - do nothing action', () => {
  const action = {
    type: 'UNKNOWN',
    result: [3]
  };
  expect(listing([], action)).toEqual([]);
});

test('reducer: posts > editState', () => {
  expect(editState({}, { type: actions.SELECT_BLOG_POST, postId: 123 }))
    .toEqual({ saving: false, persisted: true, error: false });

  expect(editState({}, { type: actions.CHANGE_BLOG_VALUE }))
    .toMatchObject({ persisted: false });

  expect(editState({}, { type: actions.FETCH }))
    .toMatchObject({ saving: true, persisted: false });

  expect(editState({}, { type: actions.BLOG_POST_UPDATED }))
    .toEqual({ saving: false, persisted: true, error: false });

  expect(editState({}, { type: actions.BLOG_POST_CREATED }))
    .toEqual({ saving: false, persisted: true, error: false });

  expect(editState({ initial: true }, { type: 'UNKNOWN' }))
    .toEqual({ initial: true });
});


test('reducer: posts > editing - selecting post should reset changes', () => {
  const action = {
    type: actions.SELECT_BLOG_POST,
    postId: 123
  };
  expect(editing({ postId: 987, anything: 'data' }, action)).toEqual({
    postId: 123
  });
});

test('reducer: posts > editing - changing values', () => {
  const action = {
    type: actions.CHANGE_BLOG_VALUE,
    property: 'property_test',
    value: 'value_test'
  };
  expect(editing({ postId: 987, property_test: 'initial' }, action)).toEqual({
    postId: 987,
    property_test: 'value_test'
  });
});

import { mdToHtml } from './markdown';

test('convertion from markdown text to html', () => {
  expect(mdToHtml('# heading')).toEqual('<h1>heading</h1>');
});


// http://stackoverflow.com/questions/3552461/how-to-format-a-javascript-date
export function formatDate(date) {
  if (!(date instanceof Date)) {
    return '-';
  }
  return date.toLocaleDateString('SK-sk', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
  });
};

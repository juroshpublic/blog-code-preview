import marked from 'marked';

const renderer = new marked.Renderer();

renderer.heading = (text, level) => `<h${level}>${text}</h${level}>`;

marked.setOptions({
  renderer,
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  smartypants: false
});

export function mdToHtml(text) {
  console.log('[markdown] Converting text to Markdown...');
  return marked(text);
};

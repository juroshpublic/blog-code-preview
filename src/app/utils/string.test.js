import { guid } from './string';

test('guid generation should be random', () => {
  const first = guid();
  const second = guid();
  expect(first).not.toEqual(second);
});


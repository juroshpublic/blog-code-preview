/**
 * Generate random unique GUID, like '85b854e4-aa5d-412b-9248-ff1e51eccebf'
 */
export function guid() {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
import { formatDate } from './date';

test('date genration should return `-` (not fail) with object not being Date', () => {
  expect(formatDate('')).toEqual('-');
  expect(formatDate(' ')).toEqual('-');
  expect(formatDate(undefined)).toEqual('-');
  expect(formatDate(null)).toEqual('-');

  // Date should pass
  expect(formatDate(new Date())).not.toEqual('-');
});

